let links = document.querySelectorAll(".close");
let verMas = document.querySelectorAll(".verMas");

links.forEach((link)=> animar(link)); 
verMas.forEach((ver)=> animar(ver));

function animar(ruta){
    let h = ruta.getAttribute("href");
    ruta.addEventListener("click",function(ev){
        ev.preventDefault();
        let content = document.querySelector(".content");
    
        content.classList.remove("animated");
        content.classList.remove("fadeInDown");
    
        content.classList.add("animated");
        content.classList.add("fadeOutUp");
    
        setTimeout(function(){
            location.href = h;
        }, 600);
        //setInterval
    
        return false;
    });
}